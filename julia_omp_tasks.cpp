#include<omp.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<stdio.h>
#include<ctime>
#include<iostream>

using namespace std;

#include "bmp.h"

typedef struct{
	double x,y;
}complex;

int **colors;
char **image;

complex add(complex a,complex b){
	complex c;
	c.x = a.x + b.x;
	c.y = a.y + b.y;
	return c;
}

complex sqr(complex a){
	complex c;
	c.x = a.x*a.x - a.y*a.y;
	c.y = 2*a.x*a.y;
	return c;
}

double mod(complex a){
	return sqrt(a.x*a.x + a.y*a.y);
}

complex mapPoint(int width,int height,double radius,int x,int y){
	complex c;
	int l = (width<height)?width:height;

	c.x = 2*radius*(x - width/2.0)/l;
	c.y = 2*radius*(y - height/2.0)/l;

	return c;
}

void colorPixel(int itr, int x, int y, int WINDOW_Y) {
    int r = colors[itr][0], g = colors[itr][1], b = colors[itr][2];

    image[x * WINDOW_Y + y][0] = (char)r;
    image[x * WINDOW_Y + y][1] = (char)g;
    image[x * WINDOW_Y + y][2] = (char)b;

}

void colorPixel(int x, int y, int WINDOW_Y) {
    image[x * WINDOW_Y + y][0] = (char)0;
    image[x * WINDOW_Y + y][1] = (char)0;
    image[x * WINDOW_Y + y][2] = (char)0;
}

void juliaSet(int width,int height,complex c,double radius,int n,int nr_threads){
	int x,y,i;
	complex z0,z1;

	omp_set_num_threads(nr_threads);

	//clock_t begin = clock();
	double wtime = omp_get_wtime ( );

	# pragma omp parallel private (x, y, i, z0, z1)
	{

		# pragma omp for
		for(x=0;x<=width;x++) {
			
			# pragma omp task
			for(y=0;y<=height;y++){
				z0 = mapPoint(width,height,radius,x,y);
				for(i=1;i<=n;i++){
					z1 = add(sqr(z0),c);
					if(mod(z1)>radius){
						//putpixel(x,y,i%15+1);
						colorPixel(i, x, y, height);
						break;
					}
					z0 = z1;
				}
				if(i>n)
					//putpixel(x,y,0);
					colorPixel(x, y, height);
			}
		}
	}

	// clock_t end = clock();
	// double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	// cout<<"OpenMP time for "<<nr_threads<<" : "<<elapsed_secs<<" seconds"<<'\n';
	wtime = omp_get_wtime ( ) - wtime;
  	cout << "\n";
  	cout << "OpenMP time on "<<nr_threads<<" threads : " << wtime << " seconds.\n";

}

void init(int ITR, int WINDOW_X, int WINDOW_Y) {
	colors = (int**)calloc(ITR+1, sizeof(int*));
    srand(time(NULL));
    for (int i = 0; i <= ITR; i++)
    {
        int *ci = (int*)calloc(3, sizeof(int));
        for (int j = 0; j < 3; j++)
            ci[j] = rand() % 255;
        colors[i] = ci;
    }

    image = (char**)calloc((WINDOW_X+1) * (WINDOW_Y+1), sizeof(char*));
    for (int i = 0; i < (WINDOW_X+1) * (WINDOW_Y+1); i++)
        image[i] = (char*)calloc(3, sizeof(char));

}

int main(int argC, char* argV[])
{
	int width, height;
	int nr_threads = 4;
	complex c;

	if(argC != 8)
		printf("Usage : <width and height of screen, real and imaginary parts of c, limit radius and iterations>");
	else{
		width = atoi(argV[1]);
		height = atoi(argV[2]);

		c.x = atof(argV[3]);
		c.y = atof(argV[4]);

		int iter = atoi(argV[6]);

		init(iter, width, height);

		//initwindow(width,height,"Julia Set");

		juliaSet(width,height,c,atof(argV[5]),atoi(argV[6]),atoi(argV[7]));

		write_bmp("out.bmp", image, width, height);
	}

	return 0;
}
