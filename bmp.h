#ifndef __BMP_H__
#define __BMP_H__

#include <string>

int write_bmp(std::string filename, char **rgb, int width, int height);

#endif
