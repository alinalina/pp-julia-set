#include<omp.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<stdio.h>
#include<ctime>
#include<iostream>
#include<string.h>

using namespace std;

struct BMPHeader
{
    char bfType[2];       /* "BM" */
    int bfSize;           /* Size of file in bytes */
    int bfReserved;       /* set to 0 */
    int bfOffBits;        /* Byte offset to actual bitmap data (= 54) */
    int biSize;           /* Size of BITMAPINFOHEADER, in bytes (= 40) */
    int biWidth;          /* Width of image, in pixels */
    int biHeight;         /* Height of images, in pixels */
    short biPlanes;       /* Number of planes in target device (set to 1) */
    short biBitCount;     /* Bits per pixel (24 in this case) */
    int biCompression;    /* Type of compression (0 if no compression) */
    int biSizeImage;      /* Image size, in bytes (0 if no compression) */
    int biXPelsPerMeter;  /* Resolution in pixels/meter of display device */
    int biYPelsPerMeter;  /* Resolution in pixels/meter of display device */
    int biClrUsed;        /* Number of colors in the color table (if 0, use
                             maximum allowed by biBitCount) */
    int biClrImportant;   /* Number of important colors.  If 0, all colors
                             are important */
};

typedef struct{
	double x,y;
}complex;

int **colors;
char **image;
bool *ready;
int myColor;

complex add(complex a,complex b){
	complex c;
	c.x = a.x + b.x;
	c.y = a.y + b.y;
	return c;
}

complex sqr(complex a){
	complex c;
	c.x = a.x*a.x - a.y*a.y;
	c.y = 2*a.x*a.y;
	return c;
}

double mod(complex a){
	return sqrt(a.x*a.x + a.y*a.y);
}

complex mapPoint(int width,int height,double radius,int x,int y){
	complex c;
	int l = (width<height)?width:height;

	c.x = 2*radius*(x - width/2.0)/l;
	c.y = 2*radius*(y - height/2.0)/l;

	return c;
}

void colorPixel(int itr, int x, int y, int WINDOW_Y) {
    int r = colors[itr][0], g = colors[itr][1], b = colors[itr][2];

    image[x * WINDOW_Y + y][0] = (char)r;
    image[x * WINDOW_Y + y][1] = (char)g;
    image[x * WINDOW_Y + y][2] = (char)b;

	ready[x * WINDOW_Y + y] = true;
	//cout << "READY: " << x * WINDOW_Y + y << " " << ready[x * WINDOW_Y + y] << endl;
	//fflush(stdout);


}

void colorPixel(int x, int y, int WINDOW_Y) {
    image[x * WINDOW_Y + y][0] = (char)0;
    image[x * WINDOW_Y + y][1] = (char)0;
    image[x * WINDOW_Y + y][2] = (char)0;


	ready[x * WINDOW_Y + y] = true;
	//cout << "READY: " << x * WINDOW_Y + y << " " << ready[x * WINDOW_Y + y] << endl;
	//fflush(stdout);
}

void juliaSet(int width,int height,complex c,double radius,int n,int nr_threads){
	int x,y,i;
	complex z0,z1;

	omp_set_num_threads(nr_threads);

	//clock_t begin = clock();
	double wtime = omp_get_wtime ( );


	# pragma omp parallel private (x, y, i, z0, z1)
	if (omp_get_thread_num() == 0) {
		//printf("Thread-ul  if %d\n", (int) omp_get_thread_num());
		fflush(stdout);

		int j, ipos;
		int bytesPerLine;
		unsigned char *line;

		FILE *file;
		struct BMPHeader bmph;

		/* The length of each line must be a multiple of 4 bytes */

		bytesPerLine = (3 * (width + 1) / 4) * 4;
		strcpy(bmph.bfType, "BM");
		bmph.bfOffBits = 54;
		bmph.bfSize = bmph.bfOffBits + bytesPerLine * height;
		bmph.bfReserved = 0;
		bmph.biSize = 40;
		bmph.biWidth = width;
		bmph.biHeight = height;
		bmph.biPlanes = 1;
		bmph.biBitCount = 24;
		bmph.biCompression = 0;
		bmph.biSizeImage = bytesPerLine * height;
		bmph.biXPelsPerMeter = 0;
		bmph.biYPelsPerMeter = 0;
		bmph.biClrUsed = 0;
		bmph.biClrImportant = 0;

		file = fopen("out.bmp", "wb");
		//if (!file)
		//	return;

		fwrite(&bmph.bfType, 2, 1, file);
		fwrite(&bmph.bfSize, 4, 1, file);
		fwrite(&bmph.bfReserved, 4, 1, file);
		fwrite(&bmph.bfOffBits, 4, 1, file);
		fwrite(&bmph.biSize, 4, 1, file);
		fwrite(&bmph.biWidth, 4, 1, file);
		fwrite(&bmph.biHeight, 4, 1, file);
		fwrite(&bmph.biPlanes, 2, 1, file);
		fwrite(&bmph.biBitCount, 2, 1, file);
		fwrite(&bmph.biCompression, 4, 1, file);
		fwrite(&bmph.biSizeImage, 4, 1, file);
		fwrite(&bmph.biXPelsPerMeter, 4, 1, file);
		fwrite(&bmph.biYPelsPerMeter, 4, 1, file);
		fwrite(&bmph.biClrUsed, 4, 1, file);
		fwrite(&bmph.biClrImportant, 4, 1, file);

		line = (unsigned char*)malloc(bytesPerLine);
		if (!line)
		{
			fprintf(stderr, "Can't allocate memory for BMP file.\n");
			//return;
		}

		for (i = height - 1; i >= 0; i--)
		{
			for (j = 0; j < width; j++)
			{
				ipos = (height * j+ i);

				while (!ready[ipos]) {}
				//printf("E GATA %d\n", ipos);
				//fflush(stdout);

				line[3*j] = image[ipos][2];
				line[3*j+1] = image[ipos][1];
				line[3*j+2] = image[ipos][0];
			}

			fwrite(line, bytesPerLine, 1, file);
		}

		free(line);
		fclose(file);
	} else {

			printf("Thread-ul else %d %d %d\n", (int) omp_get_thread_num(), width, height);
			//fflush(stdout);

			# pragma omp task
			for(x=0;x<=width;x++) {
				for(y=0;y<=height;y++){
					z0 = mapPoint(width,height,radius,x,y);
					for(i=1;i<=n;i++){
						z1 = add(sqr(z0),c);
						if(mod(z1)>radius){
							//putpixel(x,y,i%15+1);
							//printf("Colorez:%d \n", x * height + y);
							//fflush(stdout);
							colorPixel(i, x, y, height);
							break;
						}
						z0 = z1;
					}
					if(i>n) {
						//putpixel(x,y,0);

						//printf("Colorez:%d \n", x * height + y);
						colorPixel(x, y, height);
					}
				}
			}
			//fflush(stdout);
	}

	// clock_t end = clock();
	// double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	// cout<<"OpenMP time for "<<nr_threads<<" : "<<elapsed_secs<<" seconds"<<'\n';
	wtime = omp_get_wtime ( ) - wtime;
  	cout << "\n";
  	cout << "OpenMP time on "<<nr_threads<<" threads : " << wtime << " seconds.\n";

}

void init(int ITR, int WINDOW_X, int WINDOW_Y) {
	colors = (int**)calloc(ITR+1, sizeof(int*));
    srand(time(NULL));
    for (int i = 0; i <= ITR; i++)
    {
        int *ci = (int*)calloc(3, sizeof(int));
        for (int j = 0; j < 3; j++)
            ci[j] = rand() % 255;
        colors[i] = ci;
    }

    image = (char**)calloc((WINDOW_X+1) * (WINDOW_Y+1), sizeof(char*));
    for (int i = 0; i < (WINDOW_X+1) * (WINDOW_Y+1); i++)
        image[i] = (char*)calloc(3, sizeof(char));

	ready = (bool*)calloc((WINDOW_X+1) * (WINDOW_Y+1), sizeof(bool));
}

int main(int argC, char* argV[])
{
	int width, height;
	int nr_threads = 4;
	complex c;

	if(argC != 8)
		printf("Usage : <width and height of screen, real and imaginary parts of c, limit radius and iterations>");
	else{
		width = atoi(argV[1]);
		height = atoi(argV[2]);

		c.x = atof(argV[3]);
		c.y = atof(argV[4]);

		int iter = atoi(argV[6]);

		init(iter, width, height);

		//initwindow(width,height,"Julia Set");

		juliaSet(width,height,c,atof(argV[5]),atoi(argV[6]),atoi(argV[7]));

		//write_bmp("out.bmp", image, width, height);
	}

	return 0;
}
